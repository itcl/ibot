const {Builder, By, Key, until} = require('selenium-webdriver');
const Page = require('./lib/productPage');
let config = require('config');

let driver, page;

(async function botify() {
    page = new Page();
    driver = page.driver;
    try {
        await page.visit(config.get('product').url+'/'+config.get('product').slug);
        await page.getTitle().then( (t) => {
            console.log("The title is ",t)
        });
        await page.findSize(config.get('product').size);
        await page.selectSize();
        await page.addToCart();
        await page.submitCheckout();
        await page.setInput('checkout_shipping_address_first_name',config.get('account').firstname);
        await page.setInput('checkout_shipping_address_last_name',config.get('account').lastname);
        await page.setInput('checkout_shipping_address_address1',config.get('account').street + ' '+config.get('account').number);
        await page.setInput('checkout_shipping_address_city',config.get('account').street + ' '+config.get('account').city);
        await page.setInput('checkout_shipping_address_zip',config.get('account').street + ' '+config.get('account').zip);
        await page.setInput('checkout_shipping_address_phone',config.get('account').street + ' '+config.get('account').phone);
        await page.setInput('checkout_email',config.get('account').email);
        await page.submitContactForm();
        await page.submitShippingForm();

        await page.setVisaNumberInput('cc-number',config.get('creditCard').number);
        await page.setVisaNameInput('cc-name',config.get('creditCard').name);
        await page.setVisaCodeInput('cc-csc',config.get('creditCard').code);
        await page.setVisaDateInput('cc-exp',config.get('creditCard').date);

    }catch(e){
        console.log("Error Main",e)
    } finally {
        console.log("quit")
        //await driver.quit();
        //await page.quit();
    }
})();