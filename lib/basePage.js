const {Builder, By, until} = require('selenium-webdriver');
var webdriver = require('selenium-webdriver');
const chrome = require('selenium-webdriver/chrome');
var promise = require('selenium-webdriver').promise;
let o = new chrome.Options();
// o.addArguments('start-fullscreen');
o.addArguments('disable-infobars');
// o.addArguments('headless'); // Running test on visual chrome browser
o.setUserPreferences({ credential_enable_service: false });
var capabilities = {
    'browserName' : 'Chrome',
    'browser_version' : '81.0',
    'os' : 'OS X',
    'os_version' : 'Catalina',
    'resolution' : '1600x1200',
    'name' : 'Ibot-[Node] Automated sneakers web buy in'
}
var Page = function() {
    //this.driver = new Builder().setChromeOptions(o).forBrowser('chrome').build();
    this.driver = new webdriver.Builder().usingServer('http://selenium-chrome:4444/wd/hub').setChromeOptions(o).forBrowser('chrome').build();
   // withCapabilities(capabilities).
    this.webdriver = webdriver;
    this.promise = promise;
    // visit a webpage
    this.visit = async function(theUrl) {
        return await this.driver.get(theUrl);
    };

    // quit current session
    this.quit = async function() {
        return await this.driver.quit();
    };

    // wait and find a specific element with it's id
    this.findById = async function(id) {
        await this.driver.wait(until.elementLocated(By.id(id)), 15000, 'Looking for element');
        return await this.driver.findElement(By.id(id));
    };

    // wait and find a specific element with it's name
    this.findByName = async function(name) {
        await this.driver.wait(until.elementLocated(By.name(name)), 15000, 'Looking for element');
        return await this.driver.findElement(By.name(name));
    };
    this.findElements = async function(search) {
        var inputs = this.driver.findElements(this.webdriver.By.css(search));
        let arr = [];
        await inputs.then(async (elements)=>{
            var pendingHtml = await elements.map(async function (elem) {
                return elem;
            });
            arr.push(pendingHtml);
        });
        return arr;
    };
    this.findIframes = async function(search) {
        var inputs = this.driver.findElements(this.webdriver.By.tagName("iframe"));
        return inputs;
    };
    function sleep(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }
    // fill input web elements
    this.write = async function (el, txt) {
        // webdriver.executeScript("document.getElementById('elementID').setAttribute('value', 'new value for element')");
        return await el.sendKeys( txt);
    };
    // fill input web elements
    this.getTitle = async function (el, txt) {
        return await this.driver.getTitle();
    };
};

module.exports = Page;