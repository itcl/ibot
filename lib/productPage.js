let Page = require('./basePage');
const locator = require('../utils/locator');
const fake = require('../utils/fakeData');

const searchInputSelectorId = locator.searchInputSelectorId;
const searchButtonSelectorName = locator.searchButtonSelectorName;
const resultConfirmationSelectorId = locator.resultConfirmationId;
const sizeInputSelectorName = locator.sizeInputSelectorName;
const fakeNameKeyword = fake.nameKeyword;

let sizeInput, searchInput, searchButton, resultStat, checkoutButton;

Page.prototype.findSize = async function (size) {
    await this.driver.wait(this.webdriver.until.elementLocated(this.webdriver.By.css("[data-value='"+size+"']")),5000);
    sizeInput = await this.driver.findElement(this.webdriver.By.css("[data-value='"+size+"']"));
    const result = await this.driver.wait(async function () {
        const size = await sizeInput.getAttribute('data-value');
        console.log("Size found",size);
        return size;
    }, 4000);
    return result;
};
Page.prototype.addToCart = async function () {
    await this.driver.wait(this.webdriver.until.elementLocated(this.webdriver.By.css("[class='primary-btn add-to-cart']")),5000);
    sizeInput = await this.driver.findElement(this.webdriver.By.css("[class='primary-btn add-to-cart']"));
    setTimeout(function () {
        console.log("Add To card clicked");
        sizeInput.click();
    },1000);
    return 'clicked';
};
Page.prototype.submitCheckout = async function () {
    await this.driver.wait(this.webdriver.until.elementLocated(this.webdriver.By.css("[class='btn btn-solid']")),15000);
    let checkout = this.driver.findElement(this.webdriver.By.css("[class='btn btn-solid']"));
    console.log("checkout clicked");
    checkout.click();
    return "clicked"
};
Page.prototype.submitShippingForm = async function () {
    console.log(" SubmitShippingForm");
    await this.driver.wait(this.webdriver.until.elementLocated(this.webdriver.By.css("[class='radio__label__primary']")),650000);
    let button = this.driver.findElement(this.webdriver.By.css("[id='continue_button']"));
    console.log("submitShippingForm clicked");
    button.click();
    return "clicked"
};
Page.prototype.submitContactForm = async function () {
    console.log("submitContactForm");
    //await this.driver.wait(this.webdriver.until.elementTextIs(this.driver.wait(this.webdriver.until.elementLocated(this.webdriver.By.css("[id='checkout_shipping_address_phone']"))), '0032486268598'), 15000);
    await this.driver.wait(this.webdriver.until.elementLocated(this.webdriver.By.css("[id='checkout_shipping_address_phone']")),15000);
    await this.driver.wait(this.webdriver.until.elementLocated(this.webdriver.By.css("[id='continue_button']")),15000);
    let button = this.driver.findElement(this.webdriver.By.css("[id='continue_button']"));
    let phoneInput = this.driver.findElement(this.webdriver.By.css("[id='checkout_shipping_address_phone']"));
    let v = await phoneInput.getAttribute('value');
    //if(v == '0032486268598' ){
        console.log("submitContactForm clicked",v);
        button.click();
       // await this.submitShippingForm();
        return "clicked";
   // }else {
      //  console.log("what ",v)
    //}
};


Page.prototype.selectSize = async function() {
    await sizeInput.click();
    console.log("Size clicked ");
    return "clicked";
};
Page.prototype.setInput = async function (selector, email) {
    await this.driver.wait(this.webdriver.until.elementLocated(this.webdriver.By.css("[id='"+selector+"']")),15000);
    let inputClick = await this.findById(selector);
    inputClick.click();
    //let input = await this.findById(selector);
    let w = await this.write(inputClick,email);
    setTimeout(function () {
        return "setted"
    },1000)
};
Page.prototype.setVisaNumberInput = async function (selector, number) {
    console.log("set Visa number")

    await this.driver.wait(this.webdriver.until.elementLocated(this.webdriver.By.css("iframe")),15000);
    console.log("iframe found");
    let size = await this.findIframes("iframe");console.log("Iframe found=",size.length);
    await this.driver.switchTo().frame(0);
    console.log("switched")
    await this.driver.wait(this.webdriver.until.elementLocated(this.webdriver.By.css("input[autocomplete='"+selector+"']")),15000);
    let inputClick = await this.driver.findElement(this.webdriver.By.css("input[autocomplete='"+selector+"']"));
    inputClick.click();
    //let input = await this.findById(selector);
    console.log("Visa setted",number,selector)
    await this.write(inputClick,number);
    await await this.driver.switchTo().defaultContent();
    /*setTimeout(function () {

    },1000)*/
    return "setted";
};
Page.prototype.setVisaDateInput = async function (selector, number) {
    console.log("set Visa number")

    await this.driver.wait(this.webdriver.until.elementLocated(this.webdriver.By.css("iframe")),15000);
    console.log("iframe found");
    let size = await this.findIframes("iframe");console.log("Iframe found=",size.length);
    await this.driver.switchTo().frame(2);
    console.log("switched")
    await this.driver.wait(this.webdriver.until.elementLocated(this.webdriver.By.css("input[autocomplete='"+selector+"']")),15000);
    let inputClick = await this.driver.findElement(this.webdriver.By.css("input[autocomplete='"+selector+"']"));
    inputClick.click();
    //let input = await this.findById(selector);
    console.log("Visa setted",number,selector)
    await this.write(inputClick,number);
    await await this.driver.switchTo().defaultContent();
    /*setTimeout(function () {

    },1000)*/
    return "setted";
};
Page.prototype.setVisaNameInput = async function (selector, name) {
    console.log("set Visa number")

    await this.driver.wait(this.webdriver.until.elementLocated(this.webdriver.By.css("iframe")),15000);
    console.log("iframe found");
    let size = await this.findIframes("iframe");console.log("Iframe found=",size.length);
    await this.driver.switchTo().frame(1);
    console.log("switched")
    await this.driver.wait(this.webdriver.until.elementLocated(this.webdriver.By.css("input[autocomplete='"+selector+"']")),15000);
    let inputClick = await this.driver.findElement(this.webdriver.By.css("input[autocomplete='"+selector+"']"));
    inputClick.click();
    //let input = await this.findById(selector);
    console.log("Visa setted",name,selector)
    await this.write(inputClick,name);
    await await this.driver.switchTo().defaultContent();
    /*setTimeout(function () {

    },1000)*/
    return "setted";
};
Page.prototype.setVisaCodeInput = async function (selector, number) {
    console.log("set Visa number")

    await this.driver.wait(this.webdriver.until.elementLocated(this.webdriver.By.css("iframe")),15000);
    console.log("iframe found");
    let size = await this.findIframes("iframe");console.log("Iframe found=",size.length);
    await this.driver.switchTo().frame(3);
    console.log("switched")
    await this.driver.wait(this.webdriver.until.elementLocated(this.webdriver.By.css("input[autocomplete='"+selector+"']")),15000);
    let inputClick = await this.driver.findElement(this.webdriver.By.css("input[autocomplete='"+selector+"']"));
    inputClick.click();
    //let input = await this.findById(selector);
    console.log("Visa setted",number,selector)
    await this.write(inputClick,number);
    await await this.driver.switchTo().defaultContent();
    /*setTimeout(function () {

    },1000)*/
    return "setted";
};

module.exports = Page;