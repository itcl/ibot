# [Automated sneakers shop](https://medium.com/@bmshamsnahid/automated-testing-with-selenium-webdriver-and-node-js-f99f64720352)

Automated sneakers shop

[Step By Step Tutorial](https://medium.com/@bmshamsnahid/automated-testing-with-selenium-webdriver-and-node-js-f99f64720352)

## Run application
Clone the repository

```bash
git clone git@gitlab.com:itcl/ibot.git
```

Install dependencies
```bash
npm i chai@4.1.2 chai-as-promised@7.1.1 chromedriver@2.41.0 faker@4.1.0 mocha@5.2.0 mochawesome@3.0.3 selenium-webdriver@4.0.0-alpha.1 --save-dev --unsafe-perm=true --allow-root
```

Run feature.com
```bash
npm run feature.com
```

Folder Structure

    ├── ...
    │
    ├── config                         # Config directory: accounts, web shop, .....
    │   ├── feature.com.json           # Config for feature.com
    │   └── feature.com.json           # Config for feature.com
    |
    ├── lib                         # Helper methods
    │   ├── basePage.js             # Generic functionality for bying shoes
    │   └── productPage.js          # Product page functionalities
    │
    ├── test                        # Test suite
    │   └── homePage.test.js        # Testing in home page
    │
    ├── utils                       # Utility files for testing
    │   ├── fakeData.js             # Generating random keyword for searching
    │   └── locator.js              # HTML and CSS identifier for elements to test
    ├── index.js                    # Bootstrap

## License

MIT