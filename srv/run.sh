#!/usr/bin/env bash

APP_ROOT=~/www/automation
cd ${APP_ROOT}

if [ $# -eq 0 ]
  then
echo "No arguments supplied: ./run-backend.sh {production,development,staging}"
    exit
fi
export environment=$1
docker rm -f ibot
docker build --file=${APP_ROOT}"/srv/Dockerfile" --build-arg environment=$environment -t ibot/nodejs .
docker run --name ibot \
        --link selenium-chrome \
     -it ibot/nodejs:latest
#  --entrypoint ./startup-backend-$1.sh
# --restart always

